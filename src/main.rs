use clap::Parser;
use std::{io::Read, path::PathBuf, process::ExitCode, time::Duration};
use trotter::{
    error::ResponseErr,
    parse::{Gemtext, Symbol},
    Actor, Titan, UserAgent,
};

#[derive(thiserror::Error, Debug)]
enum TrotErr {
    #[error("{0}")]
    ActorErr(#[from] trotter::error::ActorError),

    #[error("{0}")]
    Response(#[from] trotter::error::ResponseErr),

    #[error("Expected one of these: archiver, indexer, researcher, webproxy")]
    BadUserAgent,

    #[error("Upload failed: {0}")]
    UploadIo(std::io::Error),

    #[error("Couldn't guess the mimetype of the file you tried to upload. Try changing its file extension.")]
    UploadMimeGuess,
}

/// 🎠 Trot: A command-line gemini client. Non-success status numbers are reflected in the exit code.
#[derive(Parser)]
struct Cli {
    /// Gemini url
    url: String,

    /// Input query (ignored when --upload is used)
    input: Option<Vec<String>>,

    /// 📁 Path to certificate pem
    #[clap(short, long)]
    cert: Option<PathBuf>,

    /// 📁 Path to key pem
    #[clap(short, long)]
    key: Option<PathBuf>,

    /// 👽 archiver, indexer, researcher, webproxy
    #[clap(long)]
    user_agent: Option<String>,

    /// ⏰ Adjust timeout in seconds (default 5)
    #[clap(short, long)]
    timeout: Option<u64>,

    /// 💾 Write output to file
    #[clap(short, long)]
    output: Option<String>,

    /// 🌕 Titan: Upload a file to this route.
    #[clap(long, short)]
    upload: Option<PathBuf>,

    /// 🌕 Titan: Specify a token to use for an upload (has no effect without the --upload flag).
    #[clap(long)]
    upload_token: Option<String>,

    /// 🚯 Only allow gemtext responses. (has no effect when using --output)
    #[clap(short, long)]
    gemtext_only: bool,

    /// 🎨 Pretty-print gemtext responses.
    #[clap(short, long)]
    pretty_print: bool,

    /// 📜 Get server's certificate pem, and exit
    #[clap(long)]
    cert_pem: bool,

    /// 📜 Get server's certificate info, and exit
    #[clap(long)]
    cert_info: bool,
}

async fn run() -> Result<(), TrotErr> {
    let Cli {
        url,
        input,
        cert,
        key,
        output,
        upload,
        upload_token,
        user_agent,
        timeout,
        gemtext_only,
        pretty_print,
        cert_pem,
        cert_info,
    } = Cli::parse();

    let mut actor = Actor {
        cert,
        key,
        ..Default::default()
    };

    // Set user agent
    if let Some(u) = user_agent {
        actor.user_agent = Some(match u.as_str() {
            "archiver" => UserAgent::Archiver,
            "indexer" => UserAgent::Indexer,
            "researcher" => UserAgent::Researcher,
            "webproxy" => UserAgent::Webproxy,
            _ => return Err(TrotErr::BadUserAgent),
        })
    };

    // Set timeout
    if let Some(t) = timeout {
        actor.timeout = Duration::from_secs(t);
    }

    // Get response
    let response = if let Some(path) = upload {
        // Titan request
        let mut file = std::fs::File::open(&path).map_err(|e| TrotErr::UploadIo(e))?;
        let mut content: Vec<u8> = Vec::new();
        file.read_to_end(&mut content)
            .map_err(|e| TrotErr::UploadIo(e))?;

        let t = Titan {
            token: upload_token,
            mimetype: mime_guess::from_path(path)
                .first()
                .ok_or(TrotErr::UploadMimeGuess)?
                .to_string(),
            content,
        };
        actor.upload(&url, t).await?
    } else {
        // Gemini request
        if let Some(input) = input {
            let mut input: String = input.iter().map(|x| format!("{x} ")).collect();
            let _ = input.pop();
            actor.input(&url, &input).await?
        } else {
            actor.get(&url).await?
        }
    };

    if cert_pem {
        println!("{}", response.certificate_pem()?);
        return Ok(());
    }

    if cert_info {
        println!("{}", response.certificate_info()?);
        return Ok(());
    }

    // Save or output
    if let Some(output) = output {
        response.save_to_path(output)?;
        return Ok(());
    }

    let text = if gemtext_only {
        response.gemtext()?
    } else {
        response.text()?
    };

    // Pretty print
    if pretty_print && response.is_gemtext() {
        for g in Gemtext::parse(&text).inner() {
            match g {
                Symbol::Text(a) => print!("{a}"),
                Symbol::Link(a, b) => print!("\x1b[0;4m{b}\x1b[0m \x1b[2m{a}"),
                Symbol::List(a) => print!("• {a}"),
                Symbol::Quote(a) => print!("\x1b[33;3;1m« {a} »"),
                Symbol::Header1(a) => print!("\x1b[32;1m▍ {a}"),
                Symbol::Header2(a) => print!("\x1b[36;1m▋ {a}"),
                Symbol::Header3(a) => print!("\x1b[34;1m█ {a}"),
                Symbol::Codeblock(a, b) => {
                    if !a.is_empty() {
                        print!("\x1b[35;2m{a}\x1b[0m\n")
                    }
                    print!("\x1b[35m{b}")
                }
            }
            println!("\x1b[0m");
        }
    } else {
        println!("{text}");
    }
    Ok(())
}

#[tokio::main]
async fn main() -> ExitCode {
    match run().await {
        Err(e) => match e {
            TrotErr::Response(ResponseErr::UnexpectedStatus(_, status, meta)) => {
                println!("{meta}");
                ExitCode::from(status.value())
            }
            _ => {
                eprintln!("🎠 Trot error :: {e}");
                ExitCode::from(1)
            }
        },
        Ok(_) => ExitCode::from(0),
    }
}
